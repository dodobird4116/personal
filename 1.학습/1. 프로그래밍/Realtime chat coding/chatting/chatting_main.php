<?php 
	session_start(); 
    //새로운 세션 시작
	if( ! isset($_SESSION['name']))
    //name이라는 세션 변수가 없으면 다음과 같은 내용을 echo를 통해 출력
	{
		echo "<script>window.alert('Login is required.');history.go(-1);</script>";
		//window.alert : 메세지창, history.go(-1) : 이전 페이지로 이동
	}
?>
<html>

<head>

<style>
.chatlogs
//.은 class를 의미하며 chatlogs라는 객체를 생성함
{
	border:4px solid #000000;
	//border : 요소의 테두리 설정, solid : 스타일 설정, 볼드와 같음
	width:400px; //가로 크기
	height:400px; //세로 크기
	background-color:#ffffff; //배경 색
	overflow:auto; //콘텐츠 내용이 박스의 크기보다 내용이 많을 경우 스크롤바가 생성되어 가로 및 세로에 필요한 부분에 생성된다
	overflow-anchor: auto; //스크롤 앵커링으로 페이지 이동 간 현재 페이지의 콘텐츠를 자유롭게 볼 수 있음
}
</style>

<script src="http://code.jquery.com/jquery-1.9.0.js"></script>
//제이쿼리는 웹사이트에 자바스크립트를 쉽게 활용할 수 있도록 도와주는 오픈소스 기반의 자바스크립트 라이브러리다
//1.xx버전은 구형 브라우저 버전을 대부분 지원하며 가장 안정적인 릴리즈로 알려져 있어 채용된 듯 싶다

<script>
//html에서 javascript를 사용하는 방법 중인 하나로 스크립트 태그를 사용
	function submitChat()
	//submitchat 이라는 함수이자 객체를 생성
	{
		if(form1.msg.value == '')
		//만약 form1의 메세지 값이 공백과 같다면
		//도트 연산자 : 객체 접근 연산자
		//==연산자 : 값이 같은지 아닌지 판별
		{
			return ;
			//반환한다
		}
		
		var msg = form1.msg.value;
		//var : vaiable의 약자로 변수를 의미함
		//form1의 메세지 출력 값을 메세지로 띄우게 하는 코드
		var xmlhttp = new XMLHttpRequest();
		//XHR : 웹브라우저와 웹서버 간 메소드가 데이터를 전송하는 객체 폼의 API(비동기 통신을 위해 브라우저에서 사용하는 Web API)
		//XMLHttpRequest 객체 생성을 의미함
		var formdata= new FormData();
		//formdata : 비동기 방식 중 이미지나 파일을 전송하고 싶을때 사용하는 코드

		formdata.append("msg",msg);
		//<input name="msg" value="msg"> 코드와 같으며 위에서 생성한 폼데이터에 값을 집어 넣는 과정이다
		form1.msg.readOnly = true;
      	//msg의 읽기 권한 설정을 지정해준다
      	//readonly 속성은 입력창을 읽기 전용으로 설정할 때 사용 - form형식에서 데이터를 담아 보낼 때 input의 특정 값은 수정하지 못하도록 제어하는 속성 중 하나
	

      	xmlhttp.onreadystatechange = function()
		// 응답이 올 때 마다 readyState는 그 상황에서의 XMLHttpRequest 상태를 저장하게 된다.
		// 즉, readyState 는 XMLHttpRequest 객체의 소유가 되는 것
		//readystatechange 이벤트는 readyState 가 변경될 때마다 발생한다.
		
		// XMLHttpRequest의 readyState 는 0 에서 4까지 순차적으로 바뀌게 된다
 		// 0 → 요청이 초기화되지 않은 상태
		// 1 → 서버 연결이 이루어지지 않음
		// 2 → 요청이 수신된 상태
		// 3 → 요청을 처리중인 상태
		// 4 → 요청의 처리가 종료되고, 응답이 준비됨.
				
		// XMLHttpRequest의 status는 HTTP Status 코드를 의미한다.
		// 200 → 올바른 응답
		// 40x → 클라이언트 측 에러 (대표:404 페이지 찾을 수 없음)
		// 50x → 서버측 에러 (500에러 → 자바에러)
		{
			if(xmlhttp.readyState == 4 && (xmlhttp.status == 200 || xmlhttp.status == 201 ) )
			//서버로부터 응답을 받았고, 그 응답이 올바른 응답일 때 해당 구문을 통해 서버로부터 응답을 가져와 DOM을 업데이트 한다는 구문
			
			//(응답의 형태는 2가지)
			//responseText 는 텍스트 형식의 응답일 경우 사용하게 되고.
			//responseXML 은 응답이 XML 형식일 경우 사용하게 된다.
			{
				document.getElementById('chatlogs').ineerHTML = xmlhttp.responseText;
				//xmlhttp.responseText : 요청에 대한 응답을 텍스트로 나타내는 string을 반환
				//XMLHttpRequest 의 open() 메소드 : open("페이지요청방식", "URL", async) 는 XMLHttpRequest 객체를 필요한 값으로 초기화하는데 사용하는 메소드
				//chatlogs의 div내 xmlhttp.responseText 결괏값을 넣게 되는 것

				form1.msg.readOnly = false;
				//form 태그 사용 시 readonly는 input 박스 value 값을 form 전송이 가능
		
			}			
		};
		xmlhttp.open('POST','insertchat.php',true);
		//클라이언트에서 서버로 요청을 보내기 위해서는 HTTP Method와 요청 URL을 정의하는 것 부터 시작해야 함
		//XMLHttpRequest의 내장 함수인 open()을 이용 POST는 리소스 생성을 의미(HTTP 요청방식), URL 지정 - php로 데이터를 보냄
		//true는 비동기방식으로 비동기호출 시 사용
		xmlhttp.send(formdata);
		//formdata를 이용하여 form 정보 전송
	}
</script>
</head>


<body>

<form name="form1">
//form1 이름의 데이터 생성
	<div class="chatlogs" id="chatlogs">
	//chatlogs 객체 생성 및 id 부여
	</div>
	Message : <input type="text" name="msg" width="400px"></textarea>
	//textarea : 글자를 입력할 수 있는 칸을 제공
	<input type="submit" class="button" value="send" onclick="submitChat()"/><br/>
	//submit : 서버로 넘겨줄수 있는 전송버튼을 생성, 객체 클릭 시 submitchat으로 이동해 해당 코드를 실행함
</form>


<script>
		
	$(document).ready(function(e)
	//html 문서 로딩이 다 끝나게 된 후 라는 문장해석
	//function(e)을 사용하는 이유는 이벤트가 발생했을 때, e라는 파라미터에 할당이 됨
	{
		$.ajaxSetup({cache:true});
		//캐싱을 사용하지 않기 위해 쓰는 코드, 한 객체(버튼)를 클릭했을때 php가 실행되도록 하는 코드에서 많이 사용
		setInterval( function(){$('#chatlogs').load('logs.php');},500 );
		//setInterval() 함수는 주기적으로 인자를 실행하는 함수
		//0.5초 간격으로 logs.php를 불러와 chatlogs에 쌓는 문장
	});
		
</script>

</body>


</html>