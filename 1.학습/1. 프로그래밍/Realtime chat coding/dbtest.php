<?php
    $link = mysqli_connect("127.0.0.1","playground_admin","password","playdround");
//웹에서 mysql로 접속하기 위한 코드로 링크 식별자에는 변수정의 $link를 사용
//mysqli_connect : 데이터베이스에 접속, localhost : 127.0.0.1, 뒤에 붙는 DB를 불러옴

    echo mysqli_get_host_info($link);
//mysql_get_host_info : MYSQL 호스트 정보를 반환함
//echo : 출력 담당, ($link) 함수로 더이상 다른 곳에서 정보를 불러오지 않음

    $sql = "show tables";
//SQL 쿼리문을 $sql 변수에 담아 실행시킴

    $result = mysqli_query($link,$sql);
//mysqli_query : mysqli_connect를 통해 연결된 객체를 이용하여 mysql쿼리 실행
//결괏값 도출을 위해 $result 함수로 반환될 배열의 형태를 결정함
    
    $row = mysqli_fetch_array($result,MYSQLI_NUM);
//mysqli_fetch_array는 type에 따라 반환될 배열의 형태가 달라지며, 테이블의 한 행에서 데이터를 읽는다
//MYSQLI_NUM : mysqli_fetch_array()에서 사용되는 PHP 상수로 숫자형 인덱스 배열을 반환하게 된다

    echo "<br><br><br><br>".$row[0];
//row의 sql 결괏값을 불러와 echo 함수로 출력
//<br> : 줄바꿈
    $row = mysqli_fetch_array($result,MYSQLI_NUM);
    echo "<br><br>".$row[0];
    $row = mysqli_fetch_array($result,MYSQLI_NUM);
    echo "<br><br>".$row[0];

    mysqli_close($link);
?>
