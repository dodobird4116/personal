<div id="container" style="width:1000px">
//<div> 태그 : 작업 시 편의상 구분용으로 존재하는 태그 (섹션 정의)
//div id, class의 차이는 중복 가능성으로 id는 html 문서 내 단한번만 사용 가능함
//html 이미지는 width와 height 속성을 가지고 있으며 이미지의 넓이와 높이값을 지정할 수 있음, px는 단위값으로 지정

<div id="header" style="background-color:#f88;height:200px;width:850px;text-align:right;">
//div태그를 사용하여 헤더 탭을 새로 만들고 스타일의 백그라운드 색상 및 위치 선정 코드 사용
//text-align 속성은 텍스트의 정렬을 뜻함

	<iframe name="userinfo_frame" src="/userinfo.php" height="100" width="800" frameborder="0" scrolling="no" 
//iframe 태그는 하나의 html 문서 내에서 다른 html 문서를 보여줘야 할 때 사용하는 태그
//기본 속성으로는 src : 삽입할 웹페이지 주소를 입력함
//frameborder 속성 : inline frame 경계선 및 두께 지정이 가능
//scrolling 속성 : yes or no로 설정가능하며 스크롤이 뜨지 않게 한 셋팅
></iframe>
 </div>
//새로운 div 태그를 사용하기 위해 마무리 지음

 <div id="menu" style="background-color:#080;height:900px;width:150px;float:left;">
 //float 속성은 요소를 어디에 배치하는지 결정하는 속성으로 해당 위치에 요소가 있으면 그 다음에 왼쪽에 바로 붙임
	
 	<iframe name="left_menu_frame" src="/leftmenu.php" height="900" width="150" frameborder="0" scrolling="no" ></iframe>
 </div>
 //새로운 iframe 태그 생성, 마무리
