<h7>
//제목 태그는 h1~h6까지 있으며 h7이상의 태그를 사용하게 되면 적용되지 않는다
//만약 존재하지 않는 태그를 사용하게 되면 일반 글꼴로 취급되게 된다
	<form method="post" action="join_process.php">
//method는 get과 post값을 지정하는 속성이고, default는 get값이 되게 된다 (전송타입설정)
//get 방식은 사용자가 보낸 데이터를 url에 전송하는 방식이다 : id,pw 쉽게 노출
//(쿼리 스트링 : 현재 페이지 정보를 다른 페이지에서는 알 수가 없으니 url 상에 물음표를 뒤에 붙여 데이터를 넘겨주는 방식)
//post 방식은 사용자가 요청 정보를 웹 서버에 보내줄 때, 자동으로 요청 정보 안에 파라미터 형식으로 데이터를 같이 끼워 전송함
//url에 정보가 노출되는 get 방식과는 다르게 데이터를 숨겨서 보내주지만 post 방식이 너무 많은 처리를 맡게 되면 과부하로 인해 속도가 느려짐

//action은 해당 저장 공간으로 전송하는 역할
//form은 해당 데이터를 웹서버에 전달해주는 역할로 쉽게 가상 저장 장소로써 이 장소에 해당 데이터를 넘겨주는 형식으로 사용
//input type에 부여할 수 있는 값 : text, password, email, sumbit, button
//고유명사 앞에 <p>태그가 필요하지 않을까? 그냥 되려나 이게
	Name : <input type="text" name="name"/><br/>
//name은 뒤에 붙는 해당 필드로 전송하라는 뜻
	ID : <input type="text" name="id"/><br/>
	PW : <input type="password" name="password"/><br/>
	PW-CHK : <input type="password" name="password-chk"/><br/>
	E-mail : <input type="text" name="email"/><br/>
	Phone-Number : <input type="text" name="phone"/><br/>
<input type="submit" value="저장"/>
//submit은 서버로 데이터를 전송해주라는 태그
</form>
</h7>