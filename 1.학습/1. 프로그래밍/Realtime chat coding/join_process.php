//만들어진 폼 안에 데이터를 추가

<?php
//<? 태그로도 php과 같은 효과를 내며 이는 php 설정 값에서 short_open_tag의 모든 값을 on으로 해주면 된다
	session_start();
//사용자 정보가 웹 서버에 없을 경우 새로운 세션을 생성하고, 이미 세션이 생성되어 있다면 기존 세션을 사용함
    unset($_SESSION['permission']);
//등록된 세션변수 permission 해제
	$link = mysqli_connect("127.0.0.1","playground_admin","password","playground");

	if($_POST['name'] == "")
//양식 제출을 올바르게 했는지 검증하는 코드, POST 방식으로 받아오며 name array를 통과하는지 검증하기 위해 사용된 것을 알 수 있음

	{
		echo "<script>alert('Name blank is required to fill.')</script>";
//php 에서 alter문으로 창 띠우기, echo는 출력
		echo "<script>history.go(-1)</script>";
//history.go(-1) : 이전페이지로 이동
	}
	else if($_POST['id'] == "")
	{
//id 입력 칸에서 다른 행위를 했을 때
		echo "<script>alert('ID blank is required to fill.')</script>";
		echo "<script>history.go(-1)</script>";
//다음과 같은 문구를 출력 및 뒤로가기 버튼 삽입
	}

	else if($_POST['password'] != $_POST['password-chk'])
//password에서 패스워드 입력 칸에서 같지 않을 때 password-chk array를 검증함
	{
		echo "<script>alert('password and password check not equal.')</script>";
		echo "<script>history.go(-1)</script>";
	}

	else
//검증하는 단계
	{
		$sql = "select * from user where id = '".$_POST['id']."'";
//user에서 id목록을 확인하는 sql문, id입력 칸은 post 방식으로 받아오게 된다 = 쿼리
		$result = mysqli_query($link, $sql);
		//SQl 쿼리를 실행하는 문장
		if(mysqli_num_rows($result) > 0)
		//집합이 1이상 있을 경우 다음과 같은 문장 출력 후 history.go를 통해 페이지 이동(뒤로가기)
		{
			echo "<script>alert('Requested ID is already existed.')</script>";
			echo "<script>history.go(-1)</script>";
		}	
	}

	$date = date("Y-m-d");
	$sql = "insert into user(name,email,phone_number,id,password,join_date,permission) values";
	//DB에 해당 정보의 값을 집어 넣음
	$sql .= "('".$_POST['name']."','".$_POST['phone']."','".$_POST['email']."','".$_POST['id']."','".$_POST['password']."','".$date."','user')";
	//각각의 속성 값에 POST 방식으로 받아오게끔 설정

	$result = mysqli_query($link,$sql);
	//sql 쿼리를 사용하기 위해 사용되는 코드로 링크 함수를 허용함
	if($result)
	{
		echo "<script>alert('Join is accepted, Thank you.');window.location.href='content.php'</script>"
	//다음과 같은 문장을 출력하며 content.php 페이지로 넘어가게 됨
	}
	else
	{
		echo "Error : ".mysqli_error($link);
	//오류 검사 구문 및 메세지 출력
	}
	
	mysqli_close($link);
	//link는 mysql 연결 객체를 뜻함, mysql 접속 종료
?>