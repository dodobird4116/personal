<h7>
    /h태그는 제목을 정의하는데 사용됨
	<form method="post" action="login_process.php">
    //form 태그는 input 요소들에 사용자가 입력한 정보들을 서버로 넘기면서 요청하는 역할을 수행함
    //전송방식은 post로 진행, 폼 내에 입력한 값들을 login_process.php 파일로 전송함

		ID : <input type="text" name="id" id="id_form"/>&nbsp;
		PW : <input type="password" name="password" id="pw_form"/>&nbsp;
        //&nbsp : 데이터 입력 간 공백 삭제를 위한 코드
        //type="text" : 텍스트 한 줄 입력 할 수 있는 텍스트박스 생성
        //type="password" : 비밀번호를 입력하는 필드 생성, 화면에 보여질때 입력이 가려져서 보여줌
        //name : 정보를 넘길 때 붙는 이름표로 한 문서 안에 여러개의 태그가 있을 경우 구분하기 위해 사용
        <input type="submit" value="login" id="login_button"/>&nbsp;
        //데이터를 제출하는 버튼이 생성되며 페이지에 표시되는 글자는 value의 login이 보이게 되는 것
        //id는 인덱스와 같은 계념
        <a href="join.php" target="content_frame">Join Us</a> 
        //타겟은 이동할 페이지가 열리는 위치를 정하는 속성으로 join us 문구와 함께 content_frame페이지로 이동
    </form>
</h7>