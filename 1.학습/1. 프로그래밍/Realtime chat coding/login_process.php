<?php
	session_start();
	//세션 시작
	unset($_SESSION['permission']);
	//permission 세션변수를 삭제함
	$link = mysqli_connect("127.0.0.1","playground_admin","password","playground");
    //MYSQL 서버에 연결해주는 함수
	$sql = "select * from user where id = '".$_POST['id']."' and password = '".$_POST['password']."'";
	//usr 테이블의 id 값을 찾는 sql 문장, ID와 PW는 POST 방식으로 받아오게 된다
	echo $sql;
	//위에서 사용된 $sql 구문을 출력하는 문장
	$result = mysqli_query($link,$sql);
	//값을 변수에 넣어 커넥트 된 db 정보와 찾고자 하는 정보를 파라미터로 받는 방식
	//연결된 DB정보와, 찾고자 하는 정보

	$row = mysqli_fetch_assoc($result);
	//연관배열을 반환할 때 사용
	echo "<br/><br/>".$row['name']."&nbsp;".$row['permission']."&nbsp;".$row['id']."&nbsp;";
	//echo : 출력, <br> 태그는 줄바꿈 처리를 함, &nbsp : 문자 사이에 공백을 넣어줌, row는 태이블 태그로 각 db에 데이터를 가져와 채움
	mysqli_close($link);
	//link는 MYSQL 연결 객체로써 DB 접속을 종료 함을 의미하는 문장
	if($row['permission'] == "admin" || $row['permission'] == "user")
	//permission 열의 데이터가 admin과 같을때, || : OR 연산자, user 데이터와 같은지 검증
	{
		$_SESSION['id'] = $row['id'];
		//id 세션에서 행에 있는 값을 받아옴
		$_SESSION['name'] = $row['name'];
		$_SESSION['permission'] = $row['permission'];
		header("Location: userinfo.php");
		//헤더 함수를 사용하여 해당 php로 이동 시킴
	}
	else
	{
		unset($_SESSION['id']);
		unset($_SESSION['name']);
		unset($_SESSION['permission']);
		//해당 세션을 모두 종료
		echo "<script>alert('Not registered Account.')</script>";
		//해당 문장을 출력
		header("Location: userinfo.php");
		//userinfo.php로 헤더 페이지 이동
	}
?>