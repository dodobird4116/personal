<?php
		session_start();
		unset($_SESSION['id']);
		unset($_SESSION['name']);
		unset($_SESSION['permission']);
		session_destroy();
		header("Location: userinfo.php");
?>
//세선을 시작하고 로그인에 사용되었던 세션들을 모두 종료한 뒤 세션을 완전히 종료, 헤더 페이지 이동