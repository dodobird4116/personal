<style>
//css 프로그래밍을 위한 스타일 태그 사용
a.button{
	-webkit-appearance: button;
    //appearance 속성은 운영체제 및 브라우저에 기본적으로 설정되어 있는 테마를 기본으로 요소를 표현함
    //웹킷은 웹 브라우저 엔진으로 웹 페이지를 랜더링하고 표시하는데 사용되는 컴포넌트이다.
    //해당 문장은 사파리와 크롬에서 구동할 수 있도록 도와주는 코드
	-moz-appearance: button;
    //파이어폭스에서 구동 될 수 있도록 사용
	appearance: button;
    //값을 올리고 내릴 수 있는 버튼의 화살표 버튼을 사용
    width:80px;
	height:30px;
    //px:픽셀의 단위, 가로 세로 폭의 값을 설정
    text-decoration: none;
    //text-decoration : 선으로 텍스트를 꾸밀 수 있게 해주는 속성이지만 none인 기본 속성을 사용하여 선을 사용하지 않음
	color: initial;
    //브라우저의 기본값인 컬러로 설정이 됨
}
</style>

<?php
	session_start();
    //세션을 새롭게 여는 문장
    if(isset($_SESSION['id']))
    //isset : 변수가 null값을 가지지 않는지 확인
    //id 라는 세션변수를 사용하며 해당 값과 일치할때 다음 문장을 출력
	{
		printf("hello %s",$_SESSION['name']);
        //%s : 문장을 출력하는 printf의 함수
?>
		<a href="logout.php" class="button">logout</a>
        //logout라는 버튼을 누르게 되면 해당 php 주소로 이동시키는 구문

<?php
	}
	else
	{
		include("login.php");
        //위에서 있었던 if문과 이어지며 다를 경우 logout.php 페이지로 이동하게 된다
	}
?>