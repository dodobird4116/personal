# Nginx guide

1. **nginx (=reverce proxy server)**

> 요청당 쓰레드를 생성하는 아파치와 다르게 <u>이벤트 구조</u>
>
> (한개의 고정 프로세스를 생성하여 사용)를 채택하여 운용



2. **structure**

* **master process**

  + 설정 파일을 읽고 유효성을 검사함

  * worker process를 관리함



* **worker process**

  * 모든 요청을 처리 함

  * 이벤트 기반의 모델을 사용하는 Nginx 가운데, 작업 프로세스 사이에 요청을 효율적으로 분배하기 위해 OS에 의존하는 매커니즘 사용

  - 작업 프로세스의 갯수는 설정 파일에서 정의되며, 정의 된 프로세스 개수와 사용 가능한 CPU 코어 숫자에 맞게 자동으로 조정

  

3. **nginx.conf**

**#core 모듈 설정 구간**

> user  nginx nginx;  //worker process가 어떤 그룹과 유저를 실행하는지 설정, 권한 설정
>
> worker_processes  1;



**#event block**

> events {
>
> ​     worker_connections 512;  // worker process가 열 수 있는 연결 수 설정 (512는 기본값)
>
> 

**\#http block**

> http {
>
> ​     include /etc/nginx/mime.types;  // include : 특정 파일을 포함하는 기능을 수행
>
> ​      default_type application/octet-stream;  // default_type : 웹서버의 기본 connect-type을 설정
>
> ​      keepalive_timeout 60;  // keepalive_timeout : 클라이언트 접속 유지 시간으로 언제 접속을 끊을지 결정
>
> ​      log_format main '$remote_addr - $remote_user [$time_local] "$request" ' // log_format : 로그 포맷 설정, $시작은 변수명으로 확인
>
> ​                      '$status $body_bytes_sent "$http_referer" '
>
> ​                      '"$http_user_agent" "$http_x_forwarded_for"';
>
> ​      access_log /var/log/nginx/access.log main;  // access_log : access 로그를 저장할 파일을 지정



**\#server block** // 한 서버에서 다른 페이지를 서비스 할 수 있도록 설정 가능

> server {
>
> ​      server_name  jeongmin-kim.com;
>
> ​      root /var/www/jeongmin-kim.com
>
> }
>
> server {
>
> ​      server_name  jeongmin.com
>
> ​      root /var/www/jeongmin.com
>
> }



_서버 블록은 여러개 사용이 가능하며, 와일드카드(*) 와 정규식 사용이 가능_
_서버 블록은 여러개 사용 할 경우, 우선순위에 따라 선택이 됨_

1. 정확한 이름
2. 별표로 시작하는 가장 긴 와일드 카드 (ex:*.example.org)
3. 별표로 끝나는 가장 긴 와일드카드 이름 (ex:example.*)
4. 첫 번째 일치하는 정규식(구성 파일에 나타나는 순서대로)



> server {
>
> ​      listen    80;
>
> ​      server_name  example.org  www.example.org;
>
>    ...
>
>  }
>
>  server {
>
> ​      listen    80;
>
> ​      server_name  *.example.org;
>
>    ...
>
>  }
>
>  server {
>
> ​      listen    80;
>
> ​      server_name  mail.*;
>
>    ...
>
>  }
>
>  server {
>
> ​      listen    80;
>
> ​      server_name  ~^(?<user>.+)\.example\.net$;
>
>    ...
>
>  }



**\#location block**

> server {
>
> ​    listen 80;
>
> ​    server_name jeongmin.com // 해당 도메인으로 요청 수신 시 location 함수 설정대로 내용을 보여줌
>
> ​    location / {
>
> ​      root html;
>
> ​      index index.html index.htm;
>
> ​    }
>
> ​    location ~* ^/(.*)/test/ {
>
> ​      root /home/test;
>
> ​    }
>
> }

