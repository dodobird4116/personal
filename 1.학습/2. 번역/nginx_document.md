https://nginx.org/en/docs/beginners_guide.html



# 1. Beginner's Guide
This guide gives a basic introduction to nginx and describes some simple tasks that can be done with it.
이 가이드는 당신에게 엔진엑스에 대한 기초 소개문을 주고, 엔진엑스로 해결할 수 있는 간단한 과제를 줍니다.

It is supposed that nginx is already installed on the reader’s machine.
읽는 사람의 기계에서 이미 ngnix가 설치되어있다고 추정합니다

If it is not, see the Installing nginx page
만약 아니라면, ngnix 설치 페이지를 통해 설치를 하고 단계를 시작해 주길 바랍니다

This guide describes how to start and stop nginx, and reload its configuration,
이 가이드에서는 어떻게 ngnix를 시작하고 종료하는지와 해당 구성을 어떻게 불러오는지에 대해 설명하고 있으며,

explains the structure of the configuration file and describes how to set up nginx to serve out static content, 
구성파일의 구조를 설명하고 있으며, 정적 콘텐츠를 구성하기 위해 ngnix를 설정하는 방법

how to configure nginx as a proxy server, and how to connect it with a FastCGI application.
ngnix 프록시 서버를 어떻게 구성하는지와 FastCGI 응용프로그램을 어떻게 연결하는 방법을 설명합니다

nginx has one master process and several worker processes. 
nginx는 한가지의 마스터 프로세스와 여러 작업 프로세스를 가지고 있습니다

The main purpose of the master process is to read and evaluate configuration, and maintain worker processes. 
마스터 프로세스의 주요 목적은 구성을 읽고 평가하는 것과 작업 프로세스를 유지합니다

Worker processes do actual processing of requests.
작업 프로세스는 요청에 대한 실제 프로세스를 수행합니다

nginx employs event-based model and OS-dependent mechanisms to efficiently distribute requests among worker processes. 
ngnix는 작업 프로세스의 요청을 효율적으로 분배하기 위해 이벤트 기반이 되는 모델과 OS에 의존하는 메커니즘을 채택합니다

The number of worker processes is defined in the configuration file and may be fixed for a given configuration or automatically adjusted to the number of available CPU cores (see worker_processes).
작업 프로세스의 수가 구성 파일에 의해 정의되며 주어진 구성에 의해 고쳐지거나 사용할 수 있는 CPU 코어의 수에 따라 자동으로 조정된다.

The way nginx and its modules work is determined in the configuration file. 
ngnix와 모듈의 작업은 구성 파일에 의해 결정됩니다

By default, the configuration file is named nginx.conf and placed in the directory
기본적으로 구성 파일의 이름은 ngnix.conf이며 해당 디렉토리에 위치하고 있습니다


## 1.1. Starting, Stopping  and Reloading Configuration

To start nginx, run the executable file. 
ngnix를 시작하려면 실행 파일을 구동하세요. 

Once nginx is started, it can be controlled by invoking the executable with the -s parameter.
ngnix를 처음 실행하게 되면 -s 명령어를 통해 실행 파일을 호출하여 제어할 수 있습니다

Use the following syntax:
해당 구문을 통해 사용할 수 있다
nginx -s signal

Where signal may be one of the following:
여기에 있는 신호들은 다음과 같을 수 있다

stop — fast shutdown // 빠른 종료
quit — graceful shutdown // 훌륭한 종료방법
reload — reloading the configuration file // 구성 파일을 다시 불러옴
reopen — reopening the log files // 로그 파일을 다시 열게 함

For example, to stop nginx processes with waiting for the worker processes to finish serving current requests, the following command can be executed:
예를들어, 작업자 프로세스가 현재 요청을 처리하기 위해 기다리면서 nginx를 중지하려면 다음과 같은 명령어를 통해 처리할 수 있다

This command should be executed under the same user that started nginx.
해당 명령어는 동일한 사용자 아래 nginx가 시작된 상태에서 실행되어야 한다

Changes made in the configuration file will not be applied until the command to reload configuration is sent to nginx or it is restarted. 
구성 파일에서 변경한 내용은 구성을 다시 불러오는 명령이 nginx로 전송되거나 다시 시작될 때 까지 적용되지 않는다.

To reload configuration, execute:
구성을 다시 불러오는 것, 실행하다


Once the master process receives the signal to reload configuration, 
마스터 프로세스가 구성을 다시 불러오기 위한 신호를 수신하면,

it checks the syntax validity of the new configuration file and tries to apply the configuration provided in it.
새로운 구성 파일의 유효성을 확인하고 그 안에 제공된 구성을 적용하려고 한다


If this is a success, 
이것이 만약 성공한다면,

the master process starts new worker processes and sends messages to old worker processes, requesting them to shut down.
마스터 프로세스는 새로운 작업 프로세스를 시작하고, 오래된 작업 프로세스에게 종료하는 메세지를 보냅니다.

Otherwise, the master process rolls back the changes and continues to work with the old configuration. 
그렇지 않으면, 마스터 프로세스는 변경되기 전으로 롤백하고 오래된 구성과 함께 작업합니다

Old worker processes, receiving a command to shut down, stop accepting new connections and continue to service current requests until all such requests are serviced.
오래된 작업 프로세스는 종료 명령을 받게되며, 새로운 연결의 수락을 중단하고, 모든 요청이 서비스될때까지 현재 요청을 계속 서비스하게 됩니다

After that, the old worker processes exit.
그러고 난 후 오래된 프로세스는 끝나게 된다

A signal may also be sent to nginx processes with the help of Unix tools such as the kill utility. 
킬 유틸리티와 같은 유닉스 도구의 도움으로 nginx 프로세스에도 신호가 전송될 수 있을것이다

In this case a signal is sent directly to a process with a given process ID. 
이 경우 신호는 주어진 프로세스 ID를 가진 프로세스로 직접 전송된다

The process ID of the nginx master process is written, by default, to the nginx.pid in the directory /usr/local/nginx/logs or /var/run. 
nginx 마스터 프로세스 ID는 기본적으로 디렉토리의 ngnix,pid에 기록됩니다

For example, if the master process ID is 1628, to send the QUIT signal resulting in nginx’s graceful shutdown
예를 들어, 만약 마스터 프로세스의 ID가 1628일때, nginx의 훌륭한 종료 신호를 보냅니다 

For getting the list of all running nginx processes, the ps utility may be used, for example, in the following way:
실행중인 nginx의 프로세스 목록을 가져오기 위해서는 ps 유틸리티를 사용할 수 있다, 예를 들어 다음과 같은 방법으로

For more information on sending signals to nginx, see Controlling nginx.
nginx로 신호를 보내기 위한 정보는 nginx 제어를 보도록 하자

## 1.2. Configuration File’s Structure

nginx consists of modules which are controlled by directives specified in the configuration file.
nginx는 구성 파일에 지정된 지시사항에 의해 제어되는 모듈로 구성되어 있다.

Directives are divided into simple directives and block directives.
지시어는 단순 지시어와 블록 지시어로 구분되어 있다.

A simple directive consists of the name and parameters separated by spaces and ends with a semicolon (;).
단순 지시어는 이름과 매게 변수를 공백으로 구분하여 구성하고 세미콜론으로 끝난다.

A block directive has the same structure as a simple directive, but instead of the semicolon it ends with a set of additional instructions surrounded by braces ({ and }). 
블록 지시자는 단순한 지시자와 동일한 구조를 가진다, 그러나 세미콜론 대신에 교정부호({})로 둘러쌓인 추가 명령어 세트로 끝이 나게 된다.


If a block directive can have other directives inside braces, it is called a context (examples: events, http, server, and location).
블록 지시문이 괄호 안에 다른 지시문을 가질 수 있다면 이를 맥락이라고 부른다 한다


## 1.3. Serving Static Content

An important web server task is serving out files (such as images or static HTML pages). 
웹서버의 중요한 작업은 파일을 제공하는 것이다 (이미지 또는 정적 HTML 페이지와 같은)

You will implement an example where, depending on the request, files will be served from different local directories: /data/www (which may contain HTML files) and /data/images (containing images). 
너는 요청에 따라 /data/www(HTML 파일을 포함할 수 있음) 와 /data/image(이미지를 포함)의 서로 다른 로컬 디렉토리에서 파일이 제공되는 예시를 구현할 것이다

This will require editing of the configuration file and setting up of a server block inside the http block with two location blocks.
위와 같은 설정을 하기 위해서는 구성 파일의 편집과 2개의 위치 블록이 있는 http 블록 내부의 서버 블록의 설정이 필요하다.

First, create the /data/www directory and put an index.html file with any text content into it and create the /data/images directory and place some images in it.
첫번째로, /data/www 디렉토리를 만들고 그 안에 텍스트 내용이 포함된 index.html 파일을 넣고, /data/imageㄴ 디렉토레를 만들고 일부 이미치를 위치시켜야 한다.

Next, open the configuration file. The default configuration file already includes several examples of the server block, mostly commented out. 
다음은, 구성 파일을 연다. 기본 구성 파일에는 이미 서버 블록의 몇 가지 예시가 포함되어 있으며, 대부분은 주석으로 표시되어있다

For now comment out all such blocks and start a new server block:
지금은 이런 모든 블록을 주석으로 표시하고 새 서버 블록을 시작해야 한다

Generally, the configuration file may include several server blocks distinguished by ports on which they listen to and by server names.
일반적으로, 그 구성 파일은 그들이 듣는 포트 및 서버 이름에 의해 구별되는 여러 서버 블록을 포함할 수 있을 것이다

Once nginx decides which server processes a request, it tests the URI specified in the request’s header against the parameters of the location directives defined inside the server block.
nginx는 요청을 처리하는 서버를 결정하게되면 요청 헤더에 지정된 URI를 서버 블록 내부에 정의된 위치 지시어의 파라미터와 비교하여 테스트한다.


This location block specifies the “/” prefix compared with the URI from the request.
이 위치은 요청에서 URI와 비교되는 "/" 접두사를 지정한다

For matching requests, the URI will be added to the path specified in the root directive, that is, to /data/www, to form the path to the requested file on the local file system. 
매칭 요청들을 위해, URI는 로컬 파일 시스템에서 요청된 파일에 대한 경로를 형성하기 위해 루트 지시어에 지시된 경로 /data/www/에 추가될 것이다

If there are several matching location blocks nginx selects the one with the longest prefix. 
일치하는 위치 블록이 여러개 있으면 nginx는 가장 긴 접두사를 가진 블록을 선택한다

The location block above provides the shortest prefix, of length one, and so only if all other location blocks fail to provide a match, this block will be used.
위치 블록은 길이가 1인 가장 짧은 접두사를 제공하므로, 다른 모든 위치 블록이 일치를 제공하지 못하는 경우에만 이 블록이 사용될 것이다

Next, add the second location block
다음, 두번째 위치 블록을 추가합니다

It will be a match for requests starting with /images/ (location / also matches such requests, but has shorter prefix).
그것은 요청을 시작하기 위한 매칭이 될 것이다 /images/ (위치 / 또한 그런 요청이 짧은 접두사를 가지고 매칭된다)

The resulting configuration of the server block should look like this:
서버 블록의 결과 구성은 다음과 같아야 합니다


This is already a working configuration of a server that listens on the standard port 80 and is accessible on the local machine at http://localhost/. 
이것은 이미 표준 포트인 80번을 수신하는 서버의 작동 구성이며 로컬 시스템에서 http://localhost/에 접근할 수 있습니다


In response to requests with URIs starting with /images/, the server will send files from the /data/images directory.
/image/로 시작하는 URI들의 요청에 응답하며, 서버는 /data/images 디렉토리로부터 파일들을 보낼 것이다


For example, in response to the http://localhost/images/example.png request nginx will send the /data/images/example.png file.
예를들어, http://localhost/images/example.png에 대한 응답으로 /data/images/example.png 파일이 전송될 것이다


If such file does not exist, nginx will send a response indicating the 404 error.
만약 각각의 파일이 존재하지 않을 때, nginx는 404 에러인 응답을 지시하여 보낸다

Requests with URIs not starting with /images/ will be mapped onto the /data/www directory.
/images/로 시작하지 않는 URI가 있는 요청은 /data/www 디렉토리에 매핑됩니다

For example, in response to the http://localhost/some/example.html request nginx will send the /data/www/some/example.html file.
예를 들어. http://localhost/some/example.html 요청에 대한 응답을 요구받았을 때 nignx는 /data/www/some/example.html 파일을 전송한다

To apply the new configuration, start nginx if it is not yet started or send the reload signal to the nginx’s master process, by executing:
새로운 구성을 적용하는 것은, nginx가 아직 시작되지 않은 경우 nginx를 시작하거나 마스터 프로세스에 다시 로딩 신호를 전송한다

In case something does not work as expected, you may try to find out the reason in access.
예상대로 작동하지 않은 일이 발생할때, 너는 access에서 이유를 찾을 수 있을 것이다

log and error.log files in the directory /usr/local/nginx/logs or /var/log/nginx.
로그와 에러로그 파일들은 /usr/local/nginx/logs 또는 /var/log/nginx 디렉토리에 있습니다.

Setting Up a Simple Proxy Server
간단한 프록시 서버를 설정하는 방법

One of the frequent uses of nginx is setting it up as a proxy server, which means a server that receives requests, passes them to the proxied servers, retrieves responses from them, and sends them to the clients.
nginx의 빈번한 사용 중 어떤 것도 그것을 프록시 서버로 설정하는 것이 아니며, 요청을 수신한 서버가 프록시 서버로 전달하는 것을 의미합니다
응답을 검색하여 클라이언트로 보냅니다

We will configure a basic proxy server, which serves requests of images with files from the local directory and sends all other requests to a proxied server.
로컬 디렉토리의 파일로 이미지 요청을 처리하고 다른 모든 요청을 프록시 서버로 전송하는 기본 프록시 서버를 구성한다

In this example, both servers will be defined on a single nginx instance.
이 예에서는 두 서버 모두 단일 nginx 인스턴스에 정의된다

First, define the proxied server by adding one more server block to the nginx’s configuration file with the following contents:
첫번째, nginx의 구성 파일에 다음과 같은 내용의 서버 블록을 하나 더 추가하여 프록시 서버를 정의한다


This will be a simple server that listens on the port 8080 (previously, the listen directive has not been specified since the standard port 80 was used) and maps all requests to the /data/up1 directory on the local file system. 
이것은 8080포트에서 듣는? 간단한 서버가 될 것이다(이전에, 표준 80포트가 사용된 이후로 수신 지시가 지정되지 않았다) 그리고 모든 요청을 로컬 파일 시스템의 /data/up1 디렉토리에 매핑한다

Create this directory and put the index.html file into it. 
이 디렉토리를 생성하고 index.html 파일을 그 안에 둡니다

Note that the root directive is placed in the server context. 
주의할점은 루트 지시자는 서버 컨텍스트에 배치 된다는 것이다

Such root directive is used when the location block selected for serving a request does not include its own root directive.
이러한 루트 지시자는 요청을 서비스하기위해 선택된 위치 블록이 자신의 루트 지시자를 포함하지 않을 때 사용 된다


Next, use the server configuration from the previous section and modify it to make it a proxy server configuration. 
다음으로는 이전 섹션의 서버 구성을 사용하고 이를 수정하여 프록시 서버 구성으로 만든다


In the first location block, put the proxy_pass directive with the protocol, name and port of the proxied server specified in the parameter (in our case, it is http://localhost:8080):
첫 번째 위치 블록에 매개변수에 지정된 프록시 서버의 프로토콜, 이름 및 포트와 함께 Proxy_pass 지시어를 입력합니다.

We will modify the second location block, which currently maps requests with the /images/ prefix to the files under the /data/images directory, to make it match the requests of images with typical file extensions. 
우리는 두번째 로케이션 블록을 수정할 것이다, 현재 /data/images 디렉토리 아래의 파일에 /images/ 접두사로 요청을 매핑하여 일반적인 파일 확장자와 이미지의 요청을 일치시킵니다.

The modified location block looks like this:
수정 된 위치 블록은 다음과 같습니다

The parameter is a regular expression matching all URIs ending with .gif, .jpg, or .png.
그 파라미터는 URL들의 마지막에 with .gif, .jpg, 또는 .png.과 같은 규칙적인 표현을 매칭합니다.

A regular expression should be preceded with ~.
규칙적인 표현 앞에는 ~ 표시가 있어야 합니다.

The corresponding requests will be mapped to the /data/images directory.
그 상응하는 요청은 /data/images 디렉토리에 매핑됩니다

In the first location block, put the proxy_pass directive with the protocol, name and port of the proxied server specified in the parameter (in our case, it is http://localhost:8080):
첫 번째 위치 블록에 매개변수에 지정된 프록시 서버의 프로토콜, 이름 및 포트와 함께 Proxy_pass 지시어를 입력합니다.

When nginx selects a location block to serve a request it first checks location directives that specify prefixes, remembering location with the longest prefix, and then checks regular expressions. 
nginx가 요청을 처리하기 위해 위치 블록을 선택할 때 먼저 접두사를 지정하는 위치 지시문을 확인하고 가장 긴 접두사가 있는 위치를 기억한 다음 정규식을 확인합니다.

If there is a match with a regular expression, nginx picks this location or, otherwise, it picks the one remembered earlier.
정규식과 일치하는 항목이 있으면 nginx는 이 위치를 선택하고, 그렇지 않으면 이전에 기억한 위치를 선택합니다.

The resulting configuration of a proxy server will look like this:
프록시 서버의 결과 구성은 다음과 같습니다.

This server will filter requests ending with .gif, .jpg, or .png and map them to the /data/images directory
이 서버는 .gif, .jpg 또는 .png로 끝나는 요청을 필터링하고 /data/images 디렉터리에 매핑합니다.

(by adding URI to the root directive’s parameter) and pass all other requests to the proxied server configured above.
(루트 지시문의 매개변수에 URI를 추가하여) 그리고 구성된 프록시 서버에 다른 모든 요청을 전달합니다.

To apply new configuration, send the reload signal to nginx as described in the previous sections.
새로운 구성을 적용하는 것은 이전 섹션에서 서술된 대로 보낸다 nginx로 다시 불러오는 신호를 전송합니다

There are many more directives that may be used to further configure a proxy connection.
프록시 연결을 추가로 구성하는 데 사용할 수 있는 더 많은 지시문이 있습니다

Setting Up FastCGI Proxying
FastCGI를 프록싱 설정하는 방법

nginx can be used to route requests to FastCGI servers which run applications built with various frameworks and programming languages such as PHP.
nginx는 다양한 프레임워크와 PHP와 같은 프로그래밍 언어로 구축된 응용 프로그램을 실행하는 FastCGI 서버로 요청을 라우팅하는 데 사용할 수 있다.

The most basic nginx configuration to work with a FastCGI server includes using the fastcgi_pass directive instead of the proxy_pass directive, and fastcgi_param directives to set parameters passed to a FastCGI server.
FastCGI 서버에서 작동하기 위한 가장 기본적인 nginx 구성에는 proxy_pass 명령어 대신 fastcgi_pass 명령어를 사용하는 것이 포함되며, fastcgi_param 명령어는 FastCGI 서버로 전달되는 파라미터를 설정합니다.

Suppose the FastCGI server is accessible on localhost:9000. 
localhost:9000에서 FastCGI서버에 접근할 수 있다고 가정합니다

Taking the proxy configuration from the previous section as a basis, replace the proxy_pass directive with the fastcgi_pass directive and change the parameter to localhost:9000.
이전 섹션의 프록시 구성을 기본적으로 사용하며, proxy_pass 지시문을 fastcgi_pass 지시문으로 바꾸고 매개 변수를 localhost:9000으로 변경합니다



In PHP, the SCRIPT_FILENAME parameter is used for determining the script name, and the QUERY_STRING parameter is used to pass request parameters.
PHP에서 SCRIPT_FILENAME 매개변수는 스크립트 이름을 결정하는 데 사용되고 QUERY_STRING 매개변수는 요청 매개변수를 전달하는 데 사용됩니다.

The resulting configuration would be:
그 결과의 구성은 이렇게 될 것이다:


This will set up a server that will route all requests except for requests for static images to the proxied server operating on localhost:9000 through the FastCGI protocol.
이를 통해 정적 이미지에 대한 요청을 제외한 모든 요청을 FastCGI 프로토콜을 통해 localhost:9000에서 동작하는 프록시 서버로 라우팅할 서버가 설정된다.

본문 번역 완료로 인해 영어로 되어있는 본문 삭제 후 문서로 정리할 예정.