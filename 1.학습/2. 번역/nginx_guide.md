https://nginx.org/en/docs/beginners_guide.html
https://icarus8050.tistory.com/57

해당 가이드는 엔진엑스의 구조와 프로세스, 그리고 nginx에서 나타나는 문제를 해결하기 위한 방법이 쓰여있다

# 1. Beginner's Guide
1. nginx에 대해 간단한 설명
 - 클라이언트로부터 요청을 받았을때, 요청에 맞는 정적 파일을 응답해주는 HTTP WEB SERVER로 활용하기도 하고
   Reverce Proxy Server로 활용하여 WAS의 서버 부하를 줄일 수 있는 로드 밸런서로 활용되기도 함

 - Apache와의 다른 점
nginx는 Event-Driven 구조로 동작하여 한개 또는 고정된 프로세스만 생성하여 사용하고
비동기 방식으로 들어오는 요청들을 동시 다발적을 처리 할 수 있음
(새로운 요청이 들어와도 프로세스와 쓰레드를 새로 생성하지 않기 때문에 적은 자원으로 효율적인 운용이 가능해짐)

 - 이벤트 기반 모델을 사용

2. nginx의 구조
- nginx는 한가지의 마스터 프로세스와 여러 작업 프로세스를 가지고 있음

*마스터 프로세스
 - 설정 파일을 읽고 유효성을 검사함
 - 작업 프로세스를 관리함

*작업 프로세스
 - 모든 요청을 처리 함
 - 이벤트 기반의 모델을 사용하는 Nginx 가운데, 작업 프로세스 사이에 요청을 효율적으로 분배하기 위해 OS에 의존하는 매커니즘 사용
 - 작업 프로세스의 갯수는 설정 파일에서 정의되며, 정의 된 프로세스 개수와 사용 가능한 CPU 코어 숫자에 맞게 자동으로 조정

*nginx는 작업 프로세스의 요청을 효울적으로 분배하기 위해
event-based(이벤트 기반) 모델, OS-Dependent mechanism(OS에 의존하는 매커니즘)을 채택

3. nginx 설정

- Nginx는 지시어에 부여하는 값에 의해 작동
- Nginx의 메인 설정 파일 경로는 /etc/nginx/nginx.conf 입니다.
(해당 경로에 파일이 없다면 /usr/local/nginx/conf/nginx.conf 또는 /usr/local/etc/nginx/nginx.conf 에 위치해있음)



## 1.1. Starting, Stopping and Reloading Configuration

| nginx -s signal
해당 명령어를 통해 실행 파일을 호출하여 제어할 수 있습니다

stop — 빠른 종료
quit — nginx가 실행되었을때 종료하는 명령어
reload — 구성 파일을 다시 불러옴
reopen — 로그 파일을 다시 열게 함

| nginx -s quit
예를들어, 작업자 프로세스가 현재 요청을 처리하기 위해 기다리면서 nginx를 중지하려면 다음과 같은 명령어를 통해 처리할 수 있다

| nginx -s reload
구성을 다시 불러오기 위한 명령어
*해당 명령어의 유의사항 및 간단히 알고 넘어가면 좋은 지식
 - 구성 파일에서 변경한 내용은 구성을 다시 불러오는 명령이 nginx로 전송되거나 다시 시작될 때 까지 적용되지 않는다
 - 마스터 프로세스가 구성을 다시 불러오기 위한 신호를 수신하면, 새로운 구성 파일의 유효성을 확인하고 그 안에 제공된 구성을 적용하려고 한다
 - 해당 작업이 성공할 경우, 마스터 프로세스는 새로운 작업 프로세스를 시작하고, 오래된 작업 프로세스에게 종료하는 메세지를 보냅니다.
(그렇지 않으면, 마스터 프로세스는 변경되기 전으로 롤백하고 오래된 구성과 함께 작업합니다)
 - 오래된 작업 프로세스는 종료 명령을 받게되며, 새로운 연결의 수락을 중단하고, 모든 요청이 서비스될때까지 현재 요청을 계속 서비스하게 됩니다

| kll -s QUIT 1628
UNIX의 명령어와 동일하게 nginx 프로세스 또한 pid를 파악하여 kill 명령어를 통해 종료할 수 있다

| ps -ax | gerp nginx
실행중인 nginx의 프로세스 목록을 불러오는 명령어 예시


## 1.2. Configuration File’s Structure

nginx는 Configuration 파일에 명시된 명령들에 의해 제어되는 모듈들로 구성되어있다

*Simple directive
 - name과 parameter로 구성
 - spaces로 구분되어있고 ;으로 끝남

*block directive
 - 문장 끝에 ;이 아닌 {}안에 추가적인 명령어들로 끝남
 - {}안에 있는 명령어들은 context로 불리며 events, http, server, location 이 있다


## 1.3. Serving Static Content

웹서버의 중요한 작업
 - 이미지 또는 정적 HTML 페이지를 제공하는 것
 - HTML이 있는 /data/www 또는 /data/images라는 로컬 디렉토리에 위치에서 제공될 것

해당과 같은 작업이 되기 위해서는 configuration file을 수정해야 함
 - http block 내의 server block의 설정이 필요
 1. /data/www 디렉토리를 만든다
 2. 그 안에 텍스트 내용이 포함된 index.html 파일을 넣는다
 3. /data/image 디렉토리를 만들고 이미지를 넣는다

다음 단계는 configuration file을 열어 수정함
 - 기본 구성 파일에는 이미 서버 블록의 몇 가지 예시가 포함되어 있으며, 다음과 같다

 http {
	server {

		}
}

- configuration file은 port로 구별되는 여러가지 server block을 포함한다
- nginx는 server name과 포트를 이용해 수신하고 있다

- nginx가 요청을 처리하는 서버를 결정하게되면
요청 헤더에 명시된 URI를 서버 블록 내부에 정의된 location directive 파라미터와 비교하여 테스트함


location / {
    root /data/www;
}

- location block은 prefix를 명시해서 요청의 URL과 비교하게 된다
- 매칭되는 요청에 대해 URI는 root에 명시되어있는 경로에 추가 될 것
(많은 location과 중복되어 매치되어 있다면, 가장 길게 매칭되는 것으로 인식하여 확인함)


location /images/ {
    root /data;
}

location block는 길이가 가장 짧은 prefix를 제공함
(다른 모든 location block이 매칭하지 못할때 사용됨)


server {
    location / {
        root /data/www;
    }

    location /images/ {
        root /data;
    }
}


해당 작업까지 하고 나면, 기존 80포트에서 수신하고 local machine에서 http://localhost로 접근 가능한 서버의 configuration이 완성된 것

- /images/로 시작하는 request에서 URI에 해당하는 응답으로, server는 /data/images directory에서 파일을 보냄
- /images/로 시작하지 않는 URI는 data/www directory 에서 매핑되어 찾게 될 것

nginx -s reload

새로운 configuration 파일이 제대로 적용되지 않았다면 nginx를 시작하거나 마스터 프로세스에 다시 로딩 신호를 보내기 위해 해당 명령어를 입력한다
(예상대로 작동하지 않은 일이 발생할때, access에서 이유를 찾을 수 있을 것)
(로그와 에러로그 파일들은 /usr/local/nginx/logs 또는 /var/log/nginx 디렉토리에 있습니다.)


## Setting Up a Simple Proxy Server

- Proxy server의 흐름
request 수신 -> proxy server 송신 -> 응답을 받은 후 client로 전송
(proxy server를 사용함으로써 서버의 IP를 숨길 수 있고 캐싱이 가능해짐)

*프록시 서버 만들기

server {
    listen 8080;
    root /data/up1;

    location / {
    }
}

- 8080포트로 수신하기 위해 설정
- 모든 요청을 /data/up1 디렉토리에 매핑
(해당 디렉토리 안에 index.html 파일을 두어야 함)



server {
    location / {
        proxy_pass http://localhost:8080;
    }

    location /images/ {
        root /data;
    }
}

proxy_pass direcive를 프로토콜과 프록시드 서버의 이름과 포트번호를 이용해 생성 (http://localhost:8080를 뜻함)


server {
    location / {
        proxy_pass http://localhost:8080/;
    }

    location ~ \.(gif|jpg|png)$ {
        root /data/images;
    }
}



## 주의사항
- 규칙적인 표현 앞에는 ~ 표시가 있어야 한다
- 해당 요청은 /data/images 디렉토리에 매핑된다


## nginx 요청 처리 우선순위
- location block의 접두사를 확인
- 가장 긴 지시문을 확인
- 정규식 확인
- 정규식과 일치하는 항목 발생 시 nginx는 해당 위치를 설정
(아닐 시 이전에 기억한 location을 선택함)


## 과정 요약
*해당 서버는 .gif .jpg .png 확장자로 끝나는 파일을 필터링하여 /data/image 디렉토리에 매핑함



## Setting Up FastCGI Proxying

*nginx는 FastCGI server에게 요청을 라우팅하기 위해 사용 될 수 있음

##FastCGI 서버에서 작동하기 위한 nginx 구성 (nginx configuration 수정)
- proxy_pass 명령어 대신 fastcgi_pass 명령어를 사용해야 함
- fastcgi_param 명령어는 FastCGI 서버로 전달되는 파라미터를 설정함

*localhost:9000에서 FastCGI서버에 접근할 수 있는 환경
- proxy_pass 지시문을 fastcgi_pass 지시문으로 바꾸고 매개 변수를 localhost:9000으로 변경

*PHP
- SCRIPT_FILENAME : 스크립트 이름을 결정
- QUERY_STRING : 요청 파라미터를 전달

server {
    location / {
        fastcgi_pass  localhost:9000;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param QUERY_STRING    $query_string;
    }

    location ~ \.(gif|jpg|png)$ {
        root /data/images;
    }
}


= 서버가 static image를 제외한 모든 요청을 locathost:9000에서 작동하는 proxy server로 FastCGI를 통해 보내도록 설정된다