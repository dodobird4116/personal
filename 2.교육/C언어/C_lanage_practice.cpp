#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma warning(disable:4996)

struct node
{
    int id;
    char name[100];
    char phone[100];
    struct node* next;
};

int menu();
void add_node(struct node *);
void delete_node(struct node*);
void insert_node(struct node*);
void print_all_nodes(struct node*);

int id = 0;

int main()
{
	int selection;
    struct node header;
    header.next = NULL;
    header.id = 0;

    while (1)
	{
        selection = menu();
        switch (selection)
        {
		case 1:
        	add_node(&header);
			break;
        case 2:
			delete_node(&header);
			break;
        case 3:
			insert_node(&header);
			break;
        case 4:
            print_all_nodes(&header);
            reak;
        default:break;
        }
    }
    return 0;
}

int menu()
{
	int selection=0;
    printf("========================\n");
	printf("[1] Add Entry\n");
    printf("[2] Delete Entry\n");
    printf("[3] Insert Entry At the specific\n");
    printf("[4] Print All Entry\n");
    printf("========================\n");
	printf("> ");
	scanf_s("%d",&selection);

    return selection;
}

void add_node(struct node *vector)
{
	char name[100];
    char phone[100];

    printf("Name : ");
    scanf("%s", name);
    printf("Phone : ");
    scanf("%s", phone);
    
	while (vector->next != NULL)
        vector = vector->next;

    vector->next = (struct node *)malloc(sizeof(struct node));
    vector = vector->next;

    vector->id = ++id;
    strcpy(vector->name,name);
    strcpy(vector->phone,phone);
    vector->next = NULL;
}

void delete_node(struct node *vector)
{
	int selection;
	struct node* temp;

    printf("Which node? : ");
    scanf("%d", &selection);

    
	while (vector->next != NULL)
    {
        if (vector->next->id == selection)
        {
			temp = vector->next;
			vector->next = vector->next->next; // unlink;
			free(temp);
			return;
        }
        vector = vector->next;
    }
    printf("Couldn't find that node...\n");
}

void insert_node(struct node* vector)
{
    int selection;
    struct node* temp;
	char name[100];
    char phone[100];

    printf("Which node? : ");
	scanf("%d", &selection);

    printf("Name : ");
	scanf("%s", name);
	printf("Phone : ");
	scanf("%s", phone);

	while (vector->next != NULL)
	{
        if (vector->id == selection)
		{
            temp = vector->next;
			vector->next = (struct node*)malloc(sizeof(struct node));
			vector = vector->next;

            vector->id = ++id;
            strcpy(vector->name, name);
			strcpy(vector->phone, phone);
            vector->next = temp;
			return;
		}
		vector = vector->next;
	}
	printf("Couldn't find that node...\n");

	puts("insert_node");
}

void print_all_nodes(struct node* vector)
{
	printf("\n\n\n\n\n========================\n");
	while (vector->next != NULL)
	{
		vector = vector->next;
		printf("%d. %s : %s\n",vector->id,vector->name,vector->phone);
	}
	printf("========================\n");

}