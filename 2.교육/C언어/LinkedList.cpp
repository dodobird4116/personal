﻿#include <stdio.h>
//studio : 표준 입출력을 의미
#include <stdlib.h>
//stdlib : 표준 라이브러리로 문자열 변환, 의사 난수 생생, 메모리 관리등의 함수를 포함
#include <string.h>
//string : 문자열 관련된 헤더
//include<>로 헤더파일 포함키기, .h는 헤더 파일의 확장자

#pragma warning(disable:4996) //visual studio가 scanf를 못쓰게해서 이를 무시하는 지시어 삽입

struct node
{
	int id;
	char name[100];
	char phone[100];
	struct node* next;
};


int menu();
void add_node(struct node *);
void delete_node(struct node*);
void insert_node(struct node*);
void print_all_nodes(struct node*);

int id = 0;

int main()
{
	int selection;
	struct node header; // 빈노드 하나를 헤더로. 
	header.next = NULL;//빈노드 없이 링크드리스트 구현할 수 있지만, 이경우 노드가 empty인지를 검사해야하는 로직이 필요
	header.id = 0;

	while (1)
	{
		selection = menu();
		switch (selection)
		{
		case 1:
			add_node(&header);
			break;
		case 2:
			delete_node(&header);
			break;
		case 3:
			insert_node(&header);
			break;
		case 4:
			print_all_nodes(&header);
			break;
		default:break;
		}

	}

	return 0;
}


int menu()
{
	int selection=0;
	printf("========================\n");
	printf("[1] Add Entry\n");
	printf("[2] Delete Entry\n");
	printf("[3] Insert Entry At the specific\n");
	printf("[4] Print All Entry\n");
	printf("========================\n");
	printf("> ");
	scanf_s("%d",&selection);

	return selection;
}

void add_node(struct node *vector)
{
	char name[100];
	char phone[100];

	printf("Name : ");
	scanf("%s", name);
	printf("Phone : ");
	scanf("%s",phone);

	while (vector->next != NULL)
		vector = vector->next; // tail을 찾음. tail의 주소를 따로 둘수 있지만, 코드가 복잡해지며 C++의 장점을 부각시키기 위해 이 방식 채택
	
	vector->next = (struct node *)malloc(sizeof(struct node));
	vector = vector->next;

	vector->id = ++id;
	strcpy(vector->name,name);
	strcpy(vector->phone,phone);
	vector->next = NULL;


}

void delete_node(struct node *vector)
{
	int selection;
	struct node* temp;

	printf("Which node? : ");
	scanf("%d", &selection);



	while (vector->next != NULL)
	{
		if (vector->next->id == selection)
		{
			temp = vector->next;
			vector->next = vector->next->next; // unlink;
			free(temp);
			return;
		}
		vector = vector->next;
	}
	printf("Couldn't find that node...\n");
}

void insert_node(struct node* vector)
{
	int selection;
	struct node* temp;
	char name[100];
	char phone[100];

	printf("Which node? : ");
	scanf("%d", &selection);

	printf("Name : ");
	scanf("%s", name);
	printf("Phone : ");
	scanf("%s", phone);
	
	while (vector->next != NULL)
	{
		if (vector->id == selection)
		{
			temp = vector->next;
			vector->next = (struct node*)malloc(sizeof(struct node));
			vector = vector->next;

			vector->id = ++id;
			strcpy(vector->name, name);
			strcpy(vector->phone, phone);
			vector->next = temp;
			return;
		}
		vector = vector->next;
	}
	printf("Couldn't find that node...\n");

	puts("insert_node");
}

void print_all_nodes(struct node* vector)
{
	printf("\n\n\n\n\n========================\n");
	while (vector->next != NULL)
	{
		vector = vector->next;
		printf("%d. %s : %s\n",vector->id,vector->name,vector->phone);
	}
	printf("========================\n");
	

}