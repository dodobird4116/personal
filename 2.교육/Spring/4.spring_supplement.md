# Mybatis

자바 오브젝트와 SQL 사이의 자동 매핑 기능을 지원하는 ORM 프레임 워크

SQL 매핑을 XML 파일에 정의하고, 이를 기반으로 SQL을 실행하여 데이터베이스와 상호 작용

- DB의 검색 조건이나 결괏값 등이 동적으로 변화할 때 유용하게 사용



#사용방법_순서

```
의존성 설정 -> DB 설정 -> MyBatis 설정 -> Mapper 인터페이스 작성 -> XML 작성 -> MyBatis 사용
```



- 동적 쿼리

실행 시점 조건에 따라 SQL 쿼리를 동적으로 생성하는 것

mybatis는 <if>, <choose>, <when>, <otherwise>, <foreach> 등의 태그 사용 가능



#.xml

```
<select id="getUserList" resultType="User">
  SELECT * FROM users
  <where>
    <if test="name != null"> // 동적 쿼리 
      AND name = #{name}
    </if>
    <if test="email != null"> // 동적 쿼리 
      AND email = #{email}
    </if>
  </where>
</select>
```

- getUserList() 메서드 호출 시 name과 email 파라미터가 null이 아니면
  해당 조건을 포함하는 SQL 쿼리를 동적으로 생성하여 조회



[단일조건] <if

```mysql
<select id="getStudentInfo" parameterType="String" resultType="hashMap">
    SELECT *
    FROM STUDENT
    WHERE USE_YN = 'Y'
    <if test='studentId != null and studentId != ""'>
        AND STUDENT_ID = #{studentId}
    </if> 
</select>
```

- 조건이 참일 때 추가될 문장 형태로 작성
- 학생 ID로 학생 정보 조회



[다중조건] <choose, <when, <otherwise

```sql
<select id="getStudentInfo" parameterType="hashMap" resultType="hashMap">
    SELECT *
    FROM BOARD
    WHERE USE_YN = 'Y'
    <choose>
        <when test='"writer".equals(searchType)'>
            AND WRITER = #{searchValue}
        </when>
        <when test='"content".equals(searchType)'>
            AND CONTENT = #{searchValue}
        </when>
        <otherwise>
            AND TITLE = #{searchValue}
        </otherwise>
    </choose>
</select>
```

- 조건1이 참일 때 추가 될 문장으로 진행
- 조건1이 거짓이고, 조건 2가 참일 때 추가될 문장
- 조건1, 조건2 모두 거짓일 때 실행할 문장
- 검색 기준에 따른 값으로 게시판 정보 조회



# Spring bean

**동일한 역할을 수행하는 객체를 여러번 만들지 않아도 되기 때문에 사용**



#IoC

스프링의 특징 중 IoC(제어의 역전) 이라는것이 있는데
원래는 사용자가 new 연산을 통해 객체를 생성하고 메소드를 호출했다면
생성된 객체의 제어권을 사용자가 아닌 스프링에게 넘겨 운영하는 것이 IoC

- 스프링에 의하여 관리당하는 자바 객체를 빈(bean)이라고 함 // **인스턴스화 된 객체**



#Controller, Service, Repository

```
Controller는 View를 이용해 화면을 나타내는 역할
Controller는 Service를 이용해서 실제 주문을 수행
Service는 Repository를 통해 데이터를 조회 또는 저장
```

- 서로 의존관계에 있는 것을 알 수 있음
- Spring에서는 이러한 관계에 있다는 상태를 개발자가 직접 나타내야 함



#Spring bean 등록 (자동과 수동으로 등록하는 방법으로 나뉨)

각 클래스에 @Component를 넣어두고 기본 패키지에 @ComponentScan을 사용하여
하위 패키지에 @Component로 등록된 컴포넌트를 스캔해 찾고,
자신이 관리할 스프링 빈 클래스로 등록해놓기에 객체를 new 연산자 없이도 자동으로 만들 수 있음

```java
@ComponentScan : @ComponentScan이 붙어있는 클래스가 있는 패키지에서부터 모든 하위 패키지의 모든 클래스를 보며 @Component가 붙은 클래스를 찾는 어노테이션
@Component : @Controller, @Service, @Repository 들은 @Component를 포함
```



# @entity

객체 중심의 매핑을 사용하는 JPA는 어노테이션이라는 것을 사용함

- DB 테이블에 대응하는 하나의 클래스
- 스프링에게 이곳은 entity 영역이라고 알려주는 어노테이션
- 어노테이션이 붙은 클래스는 JPA가 관리해주며, JPA를 사용해서 DB테이블과 매핑할 클래스는 @entity를 붙여야만이 매핑이 가능

```java
@Entity // 기본키 제약조건 추가
@Getter // 엔티티 필드 조회
@NoArgsConstructor(access = AccessLevel.PROTECTED) 
@AllArgsConstructor 
@Table(name = "USERS") 
@Builder
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
```

<center> - 회원가입에 사용되는 예시 API - </center>

**@Entity** : JPA를 사용해서 DB테이블과 매핑할 클래스는 @Entity를 붙여야만 매핑이 가능

@Getter : 엔티티의 필드를 조회할 때 사용되는 어노테이션
(Getter은 Setter와 같이 사용되는 것이 익숙한데, Getter를 단독으로 사용하는 이유는 PK(고유키)가 사용되는 엔티티가 변경될 수 있는 위험이 따르기 때문)

@NoArgsConstructor : 파라미터가 없는 기본 생성자를 자동으로 생성해주는 어노테이션
(JPA는 기본 생성자가 없으면 에러가 발생함, 엔티티에 대한 외부접근 차단을 위해 레벨 설정)

@AllArgsConstructor : 클래스의 모든 필드 값을 파라미터로 받는 생성자를 자동으로 생성해주는 어노테이션
(데이터를 집어넣는 빌더 패턴을 사용하기 위해 빌더가 전체 필드를 가진 생성자에 접근하도록 작성함)

@Table(name = "") : DB의 테이블과 연결시켜주기 위해 사용하는 어노테이션

@Builder : Lombock의 어노테이션으로 빌더 패턴을 적용할 객체에 사용



--엔티티 내부에 사용 될 어노테이션

**@Id** : 고유키를 매칭해주는 어노테이션 (Primary key)

**@GeneratedValue** : 기본키 생성을 위해 DB에 위임해주는 어노테이션





# Spring DTO

Data Transfer Object : 데이터를 이동하기 위한 객체

<img src="C:\Users\JM\AppData\Roaming\Typora\typora-user-images\image-20241011010501186.png" alt="image-20241011010501186"  />

Controller에서 Client에게 데이터를 넘겨줄 때 마다 DB에서 객체 모두를 전송할 수 없기에
필요한 정보만 담아서 DTO라는 이동수단을 통해 전달



#사용하는 이유

- View Layer와 DB Layer의 역할을 분리하기 위함
- Entity 갹체의 변경을 피하기 위함
- View와 통신하는 DTO 클래수는 자주 변경됨
- 도메인 모델링을 지키기 위함



# Lombok

여러가지 어노테이션을 제공하는 라이브러리

model 클래스나 Entity 같은 도메인 클래스 등에 반복되는 getter, setter, toString 등의 메소드를 자동으로 생성



#접근자/설정자 자동생성

@Getter, @Setter : 자동으로 생성 된 접근자와 생성자 메소드를 사용할 수 있음

```java
user.setName("홍길동");
String userName = user.getName();
```

<center> - 자동으로 메소드를 생성해주는 모습 - </center>

실전 예시)

```java
public class UserDto {
	private String id;
	private String pwd;
    
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
}
```

<center> - 직접 getter와 setter를 작성해 주었을 때 - </center>

```java
@Getter
@Setter
public class UserDto {
	private String id;
	private String pwd;
```

<center> - Lombok의 getter / setter 어노테이션 사용했을 때- </center>

- @NorgsConstructor : 파라미터가 없는 기본 생성자를 만들어줍니다.
- @AllArgsConstructor : 모든 필드 값을 파라미터로 받는 생성자를 만들어줍니다.
- @RequiredArgsConstructor : final이나 @NonNull 인 필드 값만 파라미터로 받는 생성자를 만들어줍니다.
- @EqualsAndHashCode : equals와 hashcode를 자동으로 생성해주는 어노테이션입니다.
- @Data :  (Getter, Setter, RequiredArgsConstructor, ToString, EqualsAndHashCode)를 한번에 설정해주는 어노테이션으로 실무에서는 너무 무겁고 객체의 안정성을 지키기 때문에 @Data의 활용을 지양합니다.
- @Builder : 자동으로 해당 클래스에 빌더를 추가해줍니다.

